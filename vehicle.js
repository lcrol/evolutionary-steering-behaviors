const mutationRate = 0.3;

class Vehicle {
    constructor(x, y, dna) {
        this.acc = createVector(0, 0);
        this.vel = createVector(0, -2);
        this.pos = createVector(x, y);
        this.r = 4;
        this.maxSpeed = 5;
        this.maxForce = 0.5;

        if (dna === undefined) {
            this.dna = [];
            this.dna[0] = random(-2, 2);
            this.dna[1] = random(-2, 2);
            this.dna[2] = random(0, 250);
            this.dna[3] = random(0, 250);
        } else {
            this.dna = [];
            this.dna[0] = dna[0];
            if (random(1) < mutationRate) {
                this.dna[0] += random(-0.1, 0.1);
            }
            this.dna[1] = dna[1];
            if (random(1) < mutationRate) {
                this.dna[1] += random(-0.1, 0.1);
            }
            this.dna[2] = dna[2];
            if (random(1) < mutationRate) {
                this.dna[2] += random(-10, 10);
            }
            this.dna[3] = dna[3];
            if (random(1) < mutationRate) {
                this.dna[3] += random(-10, 10);
            }
        }

        this.health = 1;
    }

    update = () => {
        this.health -= 0.005;
        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);
        this.pos.add(this.vel);
        this.acc.mult;
    };

    applyForce = (force) => {
        this.acc.add(force);
    };

    seek = (target) => {
        let desiredVect = p5.Vector.sub(target, this.pos);
        desiredVect.setMag(this.maxSpeed);
        let steeringForce = p5.Vector.sub(desiredVect, this.vel);
        return steeringForce.limit(this.maxForce);
    };

    behaviors = (positive, negative) => {
        let positiveSteer = this.eat(positive, 0.3, this.dna[2]);
        let negativeSteer = this.eat(negative, -0.75, this.dna[3]);

        positiveSteer.mult(this.dna[0]);
        negativeSteer.mult(this.dna[1]);

        this.applyForce(positiveSteer);
        this.applyForce(negativeSteer);
    };

    eat = (list, nutrition, perception) => {
        var record = Infinity;
        var closest = null;
        for (var i = list.length - 1; i >= 0; i--) {
            var d = this.pos.dist(list[i]);

            if (d < 15) {
                list.splice(i, 1);
                this.health += nutrition;
            } else {
                if (d < record && d < perception) {
                    record = d;
                    closest = list[i];
                }
            }
        }

        if (closest != null) {
            return this.seek(closest);
        }

        return createVector(0, 0);
    };

    clone = () => {
        if (random(100) < 0.1)
            return new Vehicle(this.pos.x, this.pos.y, this.dna);
        return null;
    };

    display = () => {
        let theta = this.vel.heading() + PI / 2;
        push();
        translate(this.pos.x, this.pos.y);
        rotate(theta);

        let green = color(0, 255, 0);
        let red = color(255, 0, 0);
        let col = lerpColor(red, green, this.health);

        fill(col);
        stroke(col);
        strokeWeight(1);
        beginShape();
        vertex(0, -this.r * 2);
        vertex(-this.r, this.r * 2);
        vertex(this.r, this.r * 2);
        endShape(CLOSE);
        pop();
    };

    isDead = () => {
        return this.health <= 0;
    };

    boundaries = () => {
        let d = 5;

        let desired = null;

        if (this.pos.x < d) {
            desired = createVector(this.maxSpeed, this.vel.y);
        } else if (this.pos.x > width - d) {
            desired = createVector(-this.maxSpeed, this.vel.y);
        }

        if (this.pos.y < d) {
            desired = createVector(this.vel.x, this.maxSpeed);
        } else if (this.pos.y > height - d) {
            desired = createVector(this.vel.x, -this.maxSpeed);
        }

        if (desired !== null) {
            desired.normalize();
            desired.mult(this.maxSpeed);
            let steer = p5.Vector.sub(desired, this.vel);
            steer.limit(this.maxforce);
            this.applyForce(steer);
        }
    };
}
