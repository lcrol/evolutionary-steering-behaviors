const vehicles = [];
let food = [];
let poison = [];

function setup() {
    createCanvas(windowWidth, windowHeight);

    for (let i = 0; i < 50; i++) {
        let x = random(width);
        let y = random(height);
        vehicles[i] = new Vehicle(x, y);
    }
    for (let i = 0; i < 100; i++) {
        let x = random(width);
        let y = random(height);
        food.push(createVector(x, y));
    }
    for (let i = 0; i < 100; i++) {
        let x = random(width);
        let y = random(height);
        for (let i = 0; i < 10; i++) {
            poison.push(createVector(x, y));
        }
    }
}

function mouseDragged() {
    vehicles.push(new Vehicle(mouseX, mouseY));
}

function draw() {
    background(51);

    if (random(1) < 0.5) {
        let x = random(width);
        let y = random(height);
        food.push(createVector(x, y));
    }
    if (random(1) < 0.1) {
        let x = random(width);
        let y = random(height);
        poison.push(createVector(x, y));
    }

    food.forEach((el) => {
        fill(0, 255, 0);
        noStroke();
        ellipse(el.x, el.y, 8, 8);
    });
    poison.forEach((el) => {
        fill(255, 0, 0);
        noStroke();
        ellipse(el.x, el.y, 8, 8);
    });
    // Draw an ellipse at the mouse position
    fill(127);
    stroke(200);
    strokeWeight(2);

    // Call the appropriate steering behaviors for our agents
    for (let i = vehicles.length - 1; i >= 0; i--) {
        const vehicle = vehicles[i];
        vehicle.boundaries();
        vehicle.behaviors(food, poison);
        vehicle.update();
        vehicle.display();
        const newVehicle = vehicle.clone();
        if (newVehicle !== null) vehicles.push(newVehicle);
        if (vehicle.isDead()) {
            const x = vehicle.pos.x;
            const y = vehicle.pos.y;
            food.push(createVector(x, y));
            vehicles.splice(i, 1);
        }
    }
}
